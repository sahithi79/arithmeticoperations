package com.tw.Arithmetic;

public class Arithmetic {
    public int calculateSum(int number1, int number2) {
        return number1+number2;
    }

    public int calculateDifference(int number1, int number2) {
        return number1-number2;
    }

    public int multiply(int number1,int number2) {
        return number1*number2;
    }

    public int divide(int number1, int number2) {
        return number1/number2;
    }
}
