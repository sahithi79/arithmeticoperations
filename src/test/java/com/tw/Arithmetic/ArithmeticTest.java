package com.tw.Arithmetic;


import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ArithmeticTest {
    @Nested
    public class Addition {
        @Test
        void shouldCalculateSumWhenPositiveInputsAreGiven() {
            Arithmetic arithmetic = new Arithmetic();
            int actualSum = arithmetic.calculateSum(2, 3);
            int expectedSum = 5;
            assertThat(actualSum, is(equalTo(expectedSum)));
        }

        @Test
        void shouldCalculateSumWhenNegativeInputsAreGiven() {
            Arithmetic arithmetic = new Arithmetic();
            int actualSum = arithmetic.calculateSum(-2, -3);
            int expectedSum = -5;
            assertThat(actualSum, is(equalTo(expectedSum)));
        }

        @Test
        void shouldCalculateSumWhenOnePositiveAndOneNegativeInputAreGiven() {
            Arithmetic arithmetic = new Arithmetic();
            int actualSum = arithmetic.calculateSum(-2, 3);
            int expectedSum = 1;
            assertThat(actualSum, is(equalTo(expectedSum)));
        }
    }

    @Nested
    public class Subtraction {
        @Test
        void shouldCalculateDifferenceWhenPositiveInputsAreGiven() {
            Arithmetic arithmetic = new Arithmetic();
            int actualDifference = arithmetic.calculateDifference(5, 3);
            int expectedDifference = 2;
            assertThat(actualDifference, is(equalTo(expectedDifference)));
        }

        @Test
        void shouldCalculateDifferenceWhenNegativeInputsAreGiven() {
            Arithmetic arithmetic = new Arithmetic();
            int actualDifference = arithmetic.calculateDifference(-5, -3);
            int expectedDifference = -2;
            assertThat(actualDifference, is(equalTo(expectedDifference)));
        }

        @Test
        void shouldCalculateDifferenceWhenOnePositiveAndOneNegativeInputAreGiven() {
            Arithmetic arithmetic = new Arithmetic();
            int actualDifference = arithmetic.calculateDifference(5, -3);
            int expectedDifference = 8;
            assertThat(actualDifference, is(equalTo(expectedDifference)));
        }
    }

    @Nested
    public class Multiplication{
        @Test
        void shouldReturnProductWhenTwoPositiveIntegersAreGiven(){
            Arithmetic arithmetic= new Arithmetic();
            int actualValue= arithmetic.multiply(2,3);
            int expectedValue= 6;
            assertThat(expectedValue, is(equalTo(actualValue)));
        }

        @Test
        void shouldReturnProductWhenTwoNegativeIntegersAreGiven(){
            Arithmetic arithmetic= new Arithmetic();
            int actualValue= arithmetic.multiply(-2,-3);
            int expectedValue= 6;
            assertThat(expectedValue, is(equalTo(actualValue)));
        }

        @Test
        void shouldReturnProductWhenOnePositiveAndOneNegativeAreGiven(){
            Arithmetic arithmetic= new Arithmetic();
            int actualValue= arithmetic.multiply(-2,3);
            int expectedValue= -6;
            assertThat(expectedValue, is(equalTo(actualValue)));
        }

    }

    @Nested
    public class Division{
        @Test
        void shouldReturnDivisionWhenTwoPositiveIntegersAreGiven(){
            Arithmetic arithmetic= new Arithmetic();
            int actualValue= arithmetic.divide(4,2);
            int expectedValue= 2;
            assertThat(expectedValue, is(equalTo(actualValue)));
        }

        @Test
        void shouldReturnDivisionWhenTwoNegativeIntegersAreGiven(){
            Arithmetic arithmetic= new Arithmetic();
            int actualValue= arithmetic.divide(-4,-2);
            int expectedValue= 2;
            assertThat(expectedValue, is(equalTo(actualValue)));
        }

        @Test
        void shouldReturnZeroWhenZeroIsDividedByAnyOtherInteger(){
            Arithmetic arithmetic= new Arithmetic();
            int actualValue= arithmetic.divide(0,2);
            int expectedValue= 0;
            assertThat(expectedValue, is(equalTo(actualValue)));
        }

        @Test
        void shouldReturnZeroDivideExceptionWhenDividedByZero(){
            Arithmetic arithmetic= new Arithmetic();
            assertThrows(ArithmeticException.class, () -> {
                int actualValue = arithmetic.divide(2,0);
            });
        }


    }
}
